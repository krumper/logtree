`java -jar simple-project-1.0-SNAPSHOT.jar -i <input file path> [-s||h||f]`

` -f,--find `         Makes program to ask a line to find from 'input file'
 
 `-h,--help`          prints this command
 
` -i,--input <arg>`   1 argument, 'input file' path
 
` -s,--silent `       If shouldn't not print trees(it might get too long),
                    only root hashes.

Main class `com.example.project.App`

