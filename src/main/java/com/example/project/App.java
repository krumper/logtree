package com.example.project;

import com.example.project.service.ExtractionService;
import com.example.project.service.HashTreeService;
import com.example.project.service.SignatureService;
import com.example.project.util.ArgsParser;
import com.example.project.util.TreePrinter;

import java.util.Objects;

import static com.example.project.util.HashUtils.getHash;
import static com.example.project.util.IOUtils.readFile;
import static com.example.project.util.IOUtils.readUserInput;


public class App {

    public static void main(String[] args) {

        ArgsParser props = new ArgsParser(args);

        if (props.getHelp()){
            props.getFormatter().printHelp(ArgsParser.HELP, props.getOptions());
            return;
        }

        Node root = HashTreeService.constructRootNode(readFile(props.getFilePath()));
        System.out.println("Root node hash: ".concat(root.getTip()));
        if (!props.getIsSilent()) {
            System.out.println(">>>>>>>>>>");
            TreePrinter.print(root);
            System.out.println(">>>>>>>>>>");
            System.out.println();
        }

        SignatureService.signRootNode(root);
        System.out.println();

        if (props.getQueryLine()){

            String queryLine = readUserInput();
            Node branch = ExtractionService.extractHashBranch(root.copy(), queryLine);
            if (branch == null) {
                System.out.println("Didn't find anything with line '".concat(queryLine).concat("'"));
            } else if (!props.getIsSilent()){
                System.out.println(">>>>>>>>>>");
                TreePrinter.print(branch);
                System.out.println(">>>>>>>>>>");
            } else {
                System.out.println("Found a branch: ".concat(Objects.requireNonNull(getHash(queryLine))).concat("->...->").concat(root.getTip()));

            }
        }
    }

}
