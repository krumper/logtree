package com.example.project;

import lombok.Data;

@Data
public class Node {
    private String tip;
    private Node left;
    private Node right;

    public Node copy() {
        Node n = new Node();
        n.setTip(this.tip);
        n.setLeft(this.left != null ? this.left.copy() : null);
        n.setRight(this.right != null ? this.right.copy() : null);
        return n;
    }

}