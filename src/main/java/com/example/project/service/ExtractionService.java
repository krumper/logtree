package com.example.project.service;

import com.example.project.Node;

import static com.example.project.util.HashUtils.getHash;

public class ExtractionService {

    public static Node extractHashBranch(Node trustedRoot, String lineToFind) {

        Node queryNode = new Node();
        queryNode.setTip(getHash(lineToFind));

        return findFromTrustedRoot(trustedRoot, queryNode);
    }

    private static Node findFromTrustedRoot(Node trusted, Node query) {
        if (trusted.getTip().equals(query.getTip())) {
            return trusted;
        }
        if (trusted.getLeft() != null) {
            trusted.setLeft(findFromTrustedRoot(trusted.getLeft(), query));
        }
        if (trusted.getRight() != null) {
            trusted.setRight(findFromTrustedRoot(trusted.getRight(), query));
        }
        if (trusted.getLeft() == null && trusted.getRight() == null) {
            return null;
        }
        return trusted;
    }

}
