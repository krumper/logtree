package com.example.project.service;

import com.example.project.Node;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.project.util.HashUtils.getHash;

public class HashTreeService {

    public static Node constructRootNode(List<String> lines) {
        if (lines == null || lines.isEmpty()) {
            return null;
        }

        List<Node> hashes = lines.stream()
                .map(e -> {
                    Node n = new Node();
                    n.setTip(getHash(e));
                    return n;
                })
                .collect(Collectors.toList());

        if (hashes.size() == 1) {
            return hashes.get(0);
        }

        List<Node> parents = getParents(hashes);
        while (parents.size() > 1) {
            parents = getParents(parents);
        }

        return parents.get(0);
    }


    private static List<Node> getParents(List<Node> hashes) {
        List<Node> parents = new ArrayList<>();

        for (int i = 0; i < hashes.size() - 1; i += 2) {
            Node parent = new Node();
            parent.setLeft(hashes.get(i));
            parent.setRight(hashes.get(i + 1));
            parent.setTip(getHash(parent.getLeft().getTip()
                    .concat(parent.getRight().getTip())));
            parents.add(parent);
        }

        if (hashes.size() % 2 == 1) {
            parents.add(hashes.get(hashes.size() - 1));
        }

        return parents;
    }

}
