package com.example.project.service;

import com.example.project.Node;
import com.guardtime.ksi.Signer;
import com.guardtime.ksi.exceptions.KSIException;
import com.guardtime.ksi.unisignature.KSISignature;

import java.nio.charset.Charset;

public class SignatureService {

    private static Signer signer;

    public static void signRootNode(Node root) {
        System.out.println("Tipuräsi signeerimiseks võid kirjutada ilma sisuta meetodi. /.../ \n" +
                "Boonuspunktide saamiseks võid enda koodile lisada meie SDK ja signeerimist teostavasse\n" +
                "meetodisse jätta väljakommenteerituna pöördumise selle poole.");

//        try {
//            guardTimeSdkSign(root.getTip());
//        } catch (KSIException e) {
//            System.err.println("Couldn't sign");
//            e.printStackTrace();
//        }
    }

    public static void guardTimeSdkSign(String strToSign) throws KSIException {
        // https://github.com/guardtime/ksi-sdk-samples/blob/e7ce411b21d056bb8b724043356e1644886e0ce8/java-sdk/src/test/java/com/guardtime/ksi/samples/SigningSamples.java#L72

        // Whenever signing text data, make sure you control and know what the character set
        // (encoding) was otherwise you may have trouble in the verification later.
        byte[] document = strToSign.getBytes(Charset.forName("UTF-8"));

        // Sign it, the hash of the document is computed implicitly by the sign method
        @SuppressWarnings("unused")
        KSISignature signature = signer.sign(document);

        // Persist signature to file
        // signature.writeTo(...);
    }
}
