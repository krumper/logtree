package com.example.project.util;

import lombok.Getter;
import org.apache.commons.cli.*;

import java.util.stream.Stream;

@Getter
public class ArgsParser {

    public static final String HELP = "java -jar simple-project-1.0-SNAPSHOT.jar -i <input file> [-s||-h||-f]";
    public static final String NO_ARGS = "Invalid args";

    private HelpFormatter formatter = new HelpFormatter();
    private Options options = new Options();

    private String filePath;
    private Boolean queryLine = false;
    private Boolean isSilent = false;
    private Boolean help = false;

    public ArgsParser(String[] args){
        Option input = new Option("i", "input", true,
                "1 argument, 'input file' path");
        input.setRequired(true);
        input.setArgs(1);

        Option silent = new Option("s", "silent", true,
                "If shouldn't not print trees(it might get too long), only root hashes.");
        silent.setRequired(false);
        silent.setArgs(0);

        Option find = new Option("f", "find", true,
                "Makes program to ask a line to find from 'input file'");
        find.setRequired(false);
        find.setArgs(0);

        Option help = new Option("h", "help", false,
                "prints this command");
        help.setRequired(false);

        Stream.of(input, find, silent, help).forEach(options::addOption);

        CommandLine cmd;

        try{
            cmd = new DefaultParser().parse(options, args);
            if (!cmd.hasOption("i")){
                terminate(NO_ARGS);
            }
            this.filePath = cmd.getOptionValue("i");
            this.queryLine = cmd.hasOption("f") || cmd.hasOption("find");
            this.isSilent = cmd.hasOption("s") || cmd.hasOption("silent");
            this.help = (cmd.hasOption("h") || cmd.hasOption("help"));
        } catch (ParseException e) {
            formatter.printHelp(HELP, options);
            e.printStackTrace();
            terminate(NO_ARGS);
        }
    }

    private void terminate(String msg){
        throw new RuntimeException(msg);
    }
}
