package com.example.project.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IOUtils {

    private IOUtils() {
    }

    public static String readUserInput() {
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter a line to find: ");
        String n = reader.nextLine();
        reader.close();
        return n;
    }

    public static List<String> readFile(String filePath) {
        List<String> lines = new ArrayList<>();
        try {
            Files.lines(Paths.get(filePath)).forEach(lines::add);
        } catch (IOException e) {
            System.err.println("Couldn't read file");
            e.printStackTrace();
        }
        return lines;
    }

}
