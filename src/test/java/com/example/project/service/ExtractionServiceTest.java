package com.example.project.service;

import com.example.project.Node;
import com.example.project.util.TreePrinter;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.example.project.util.HashUtils.getHash;
import static org.junit.Assert.assertEquals;

public class ExtractionServiceTest {

    @Test
    public void extractNode(){
        List<String> lines = Arrays.asList(
                "15:16:38,008 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback-test.xml]",
                "15:16:38,008 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback.groovy]",
                "15:16:38,009 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Found resource [logback.xml] at [file:/Users/vladalenitsev/coding/elering/backend/target/repositoorium-0.0.1/WEB-INF/classes/logback.xml]"
        );

        Node expected = HashTreeService.constructRootNode(lines);
        Node extracted = ExtractionService.extractHashBranch(expected, lines.get(1));

        expected.setRight(null);
        expected.getLeft().setLeft(null);

        assertEquals(expected, extracted);
    }

}
