package com.example.project.service;

import com.example.project.Node;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.example.project.util.HashUtils.getHash;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class HashTreeServiceTest {

    @Test
    public void buildNullNode(){
        Node actual = HashTreeService.constructRootNode(new ArrayList<>());
        assertNull(actual);
    }

    @Test
    public void buildTreeFromOneLineSuccess() {
        String line = "One line";
        Node actual = HashTreeService.constructRootNode(Collections.singletonList(line));
        Node expected = new Node();
        expected.setTip(getHash(line));
        assertEquals(expected, actual);
    }

    @Test
    public void buildTreeFromMultipleLines() {
        List<String> lines = Arrays.asList(
                "15:16:38,008 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback-test.xml]",
                "15:16:38,008 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Could NOT find resource [logback.groovy]",
                "15:16:38,009 |-INFO in ch.qos.logback.classic.LoggerContext[default] - Found resource [logback.xml] at [file:/Users/vladalenitsev/coding/elering/backend/target/repositoorium-0.0.1/WEB-INF/classes/logback.xml]"
                );

        Node actual = HashTreeService.constructRootNode(lines);

        Node leaf1 = new Node();
        Node leaf2 = new Node();
        Node leaf3 = new Node();
        Node parent1 = new Node();
        Node parent2 = new Node();
        Node root = new Node();

        leaf1.setTip(getHash(lines.get(0)));
        leaf2.setTip(getHash(lines.get(1)));
        leaf3.setTip(getHash(lines.get(2)));

        parent1.setLeft(leaf1);
        parent1.setRight(leaf2);

        parent2.setTip(leaf3.getTip());
        parent1.setTip(getHash(leaf1.getTip().concat(leaf2.getTip())));

        root.setLeft(parent1);
        root.setRight(parent2);
        root.setTip(getHash(parent1.getTip().concat(parent2.getTip())));

        assertEquals(root, actual);

    }


}
